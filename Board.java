public class Board{
	private Die die1;
	private Die die2;
	private boolean[] closedTiles;
	
	public Board(){
		this.die1 = new Die();
		this.die2 = new Die();
		this.closedTiles = new boolean [12];
	}
	
	public String toString(){
		String board = "";
		for (int i = 0; i < this.closedTiles.length; i++){
			if (this.closedTiles[i] == false){
				int position = i + 1;
				board = board+" "+position;
			}
			else {
				board = board+" X";
			}
		}
		return board;
	}
	
	public boolean playATurn(){
		this.die1.roll();
		this.die2.roll();
		System.out.println(this.die1+" "+this.die2);
		int sum = this.die1.getPips() + this.die2.getPips();
		if (this.closedTiles[sum-1] == false){
			this.closedTiles[sum-1] = true;
			System.out.println("Closing tile "+sum);
			return false;
		}
		else {
			System.out.println("The tile at the position "+sum+" is already shut");
			return true;
		}
	}
}
			