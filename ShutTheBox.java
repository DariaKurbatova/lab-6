public class ShutTheBox{
	public static void main(String[] args){
		System.out.println("Welcome to the shut the box game!");
		
		Board gameBoard = new Board();
		boolean gameOver = false;
		
		while ( gameOver == false) {
			System.out.println("Player 1's turn");
			System.out.println(gameBoard);
			boolean turn = gameBoard.playATurn();
			if (turn == true){
				System.out.println("Player 2 wins!");
				gameOver = true;
			}
			else {
				System.out.println("Player 2's turn");
				System.out.println(gameBoard);
				turn = gameBoard.playATurn();
				if (turn == true){
					System.out.println("Player 1 wins!");
					gameOver = true;
				}
				else{
					gameOver = false;
					}
			}
		}
		
	}
}